﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core5.TopSkyHotelManagerSystem
{
    public class UserAppService:TopSkyHotelManagerSystemAppServiceBase,IUserAppService
    {
        private readonly IRepository<User> userRepository;

        public UserAppService(IRepository<User> userRepository)
        {
            this.userRepository = userRepository;
        }

        /// <summary>
        /// 获取所有用户信息
        /// </summary>
        /// <returns></returns>
        public async Task<List<User>> GetUsers()
        {
            var listUser = await userRepository.GetAllListAsync();
            return listUser;
        }
    }
}
