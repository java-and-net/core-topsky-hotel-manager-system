﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core5.TopSkyHotelManagerSystem
{
    public interface IUserAppService:IApplicationService
    {
        /// <summary>
        /// 获取所有用户信息
        /// </summary>
        /// <returns></returns>
        Task<List<User>> GetUsers();
    }
}
