﻿using Abp.Application.Services;

namespace Core5.TopSkyHotelManagerSystem
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    public abstract class TopSkyHotelManagerSystemAppServiceBase : ApplicationService
    {
        protected TopSkyHotelManagerSystemAppServiceBase()
        {
            LocalizationSourceName = TopSkyHotelManagerSystemConsts.LocalizationSourceName;
        }
    }
}