﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;

namespace Core5.TopSkyHotelManagerSystem
{
    [DependsOn(
        typeof(TopSkyHotelManagerSystemCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class TopSkyHotelManagerSystemApplicationModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(TopSkyHotelManagerSystemApplicationModule).GetAssembly());
        }
    }
}