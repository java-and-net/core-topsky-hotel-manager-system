﻿using Abp.AspNetCore.Mvc.Views;

namespace Core5.TopSkyHotelManagerSystem.Web.Views
{
    public abstract class TopSkyHotelManagerSystemRazorPage<TModel> : AbpRazorPage<TModel>
    {
        protected TopSkyHotelManagerSystemRazorPage()
        {
            LocalizationSourceName = TopSkyHotelManagerSystemConsts.LocalizationSourceName;
        }
    }
}
