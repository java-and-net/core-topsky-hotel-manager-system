﻿using Abp.AspNetCore;
using Abp.AspNetCore.Configuration;
using Abp.Configuration.Startup;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Core5.TopSkyHotelManagerSystem.Configuration;
using Core5.TopSkyHotelManagerSystem.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.Extensions.Configuration;

namespace Core5.TopSkyHotelManagerSystem.Web.Startup
{
    [DependsOn(
        typeof(TopSkyHotelManagerSystemApplicationModule), 
        typeof(TopSkyHotelManagerSystemEntityFrameworkCoreModule), 
        typeof(AbpAspNetCoreModule))]
    public class TopSkyHotelManagerSystemWebModule : AbpModule
    {
        private readonly IConfigurationRoot _appConfiguration;

        public TopSkyHotelManagerSystemWebModule(IWebHostEnvironment env)
        {
            _appConfiguration = AppConfigurations.Get(env.ContentRootPath, env.EnvironmentName);
        }

        public override void PreInitialize()
        {
            Configuration.Modules.AbpWebCommon().SendAllExceptionsToClients = true;//向前端返回完整错误日志

            Configuration.DefaultNameOrConnectionString = _appConfiguration.GetConnectionString(TopSkyHotelManagerSystemConsts.ConnectionStringName);

            Configuration.Navigation.Providers.Add<TopSkyHotelManagerSystemNavigationProvider>();

            Configuration.Modules.AbpAspNetCore()
                .CreateControllersForAppServices(
                    typeof(TopSkyHotelManagerSystemApplicationModule).GetAssembly()
                );
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(TopSkyHotelManagerSystemWebModule).GetAssembly());
        }

        public override void PostInitialize()
        {
            IocManager.Resolve<ApplicationPartManager>()
                .AddApplicationPartsIfNotAddedBefore(typeof(TopSkyHotelManagerSystemWebModule).Assembly);
        }
    }
}
