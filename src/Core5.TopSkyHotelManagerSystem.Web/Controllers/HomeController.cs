using Microsoft.AspNetCore.Mvc;

namespace Core5.TopSkyHotelManagerSystem.Web.Controllers
{
    public class HomeController : TopSkyHotelManagerSystemControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}