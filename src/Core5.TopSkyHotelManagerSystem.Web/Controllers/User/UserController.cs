﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core5.TopSkyHotelManagerSystem.Web.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class UserController : TopSkyHotelManagerSystemControllerBase
    {
        private readonly IUserAppService userAppService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userAppService"></param>
        public UserController(IUserAppService userAppService)
        {
            this.userAppService = userAppService;
        }

        /// <summary>
        /// 获取所有用户信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public Task<List<User>> GetUsers()
        {
            return this.userAppService.GetUsers();
        }

    }
}
