using Abp.AspNetCore.Mvc.Controllers;

namespace Core5.TopSkyHotelManagerSystem.Web.Controllers
{
    public abstract class TopSkyHotelManagerSystemControllerBase: AbpController
    {
        protected TopSkyHotelManagerSystemControllerBase()
        {
            LocalizationSourceName = TopSkyHotelManagerSystemConsts.LocalizationSourceName;
        }
    }
}