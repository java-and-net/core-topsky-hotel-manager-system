﻿using Abp.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Core5.TopSkyHotelManagerSystem.EntityFrameworkCore
{
    public class TopSkyHotelManagerSystemDbContext : AbpDbContext
    {
        //Add DbSet properties for your entities...

        public virtual DbSet<User> Users { get; set; }
        public TopSkyHotelManagerSystemDbContext(DbContextOptions<TopSkyHotelManagerSystemDbContext> options) 
            : base(options)
        {

        }
    }
}
