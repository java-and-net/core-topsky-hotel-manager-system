﻿using Abp.EntityFrameworkCore;
using Abp.Modules;
using Abp.Reflection.Extensions;

namespace Core5.TopSkyHotelManagerSystem.EntityFrameworkCore
{
    [DependsOn(
        typeof(TopSkyHotelManagerSystemCoreModule), 
        typeof(AbpEntityFrameworkCoreModule))]
    public class TopSkyHotelManagerSystemEntityFrameworkCoreModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(TopSkyHotelManagerSystemEntityFrameworkCoreModule).GetAssembly());
        }
    }
}