﻿using Microsoft.EntityFrameworkCore;

namespace Core5.TopSkyHotelManagerSystem.EntityFrameworkCore
{
    public static class DbContextOptionsConfigurer
    {
        public static void Configure(
            DbContextOptionsBuilder<TopSkyHotelManagerSystemDbContext> dbContextOptions, 
            string connectionString
            )
        {
            /* This is the single point to configure DbContextOptions for TopSkyHotelManagerSystemDbContext */
            dbContextOptions.UseMySql(connectionString,new MySqlServerVersion("8.0.22"));
        }
    }
}
