﻿using Core5.TopSkyHotelManagerSystem.Configuration;
using Core5.TopSkyHotelManagerSystem.Web;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace Core5.TopSkyHotelManagerSystem.EntityFrameworkCore
{
    /* This class is needed to run EF Core PMC commands. Not used anywhere else */
    public class TopSkyHotelManagerSystemDbContextFactory : IDesignTimeDbContextFactory<TopSkyHotelManagerSystemDbContext>
    {
        public TopSkyHotelManagerSystemDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<TopSkyHotelManagerSystemDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            DbContextOptionsConfigurer.Configure(
                builder,
                configuration.GetConnectionString(TopSkyHotelManagerSystemConsts.ConnectionStringName)
            );

            return new TopSkyHotelManagerSystemDbContext(builder.Options);
        }
    }
}