﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core5.TopSkyHotelManagerSystem
{
    [Table("users")]
    public class User:Entity
    {
        /// <summary>
        /// 编号
        /// </summary>
        public virtual int Id { get; set; }

        [Required]
        public virtual string name { get; set; }

        [Required]
        public virtual int age { get; set; }
    }
}
