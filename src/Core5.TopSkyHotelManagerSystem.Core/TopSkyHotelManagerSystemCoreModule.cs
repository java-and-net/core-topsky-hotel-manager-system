﻿using Abp.Modules;
using Abp.Reflection.Extensions;
using Core5.TopSkyHotelManagerSystem.Localization;

namespace Core5.TopSkyHotelManagerSystem
{
    public class TopSkyHotelManagerSystemCoreModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Auditing.IsEnabledForAnonymousUsers = true;

            TopSkyHotelManagerSystemLocalizationConfigurer.Configure(Configuration.Localization);
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(TopSkyHotelManagerSystemCoreModule).GetAssembly());
        }
    }
}