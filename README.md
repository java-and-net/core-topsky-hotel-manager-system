<h1 align="center"><img src="https://foruda.gitee.com/avatar/1677165732744604624/7158691_java-and-net_1677165732.png!avatar100" alt="组织logo.png" /></h1>
<h1 align="center">Core5.TopSkyHotelManagerSystem</h1>
<p align="center">
	<a href='https://gitee.com/java-and-net/core-topsky-hotel-manager-system/stargazers'><img src='https://gitee.com/java-and-net/core-topsky-hotel-manager-system/badge/star.svg?theme=dark' alt='star'></img></a>
        <a href='https://img.shields.io/travis/antvis/g2.svg'><img src="https://img.shields.io/travis/antvis/g2.svg" alt=""></img>
        <a href='https://img.shields.io/badge/license-MIT-000000.svg'><img src="https://img.shields.io/badge/license-MIT-000000.svg" alt=""></img></a>
        <a href='https://img.shields.io/badge/language-C#-red.svg'><img src="https://img.shields.io/badge/language-CSharp-red.svg" alt=""></img></a>
</p>

## 项目介绍
.NET 5版本，基于ASP.NET Boilerplate框架进行开发，针对中小型酒店设计的管理系统，团队项目，参与者请查看README.md。

## 使用方法
- 第一步：git clone https://gitee.com/java-and-net/core-topsky-hotel-manager-system.git

- 第二步：安装.NET5.0 SDK和运行时，使用VS2019 v16.9.4以上版本打开Core5.TopSkyHotelManagerSystem.sln

- 第三步：还原Nuget包，并在appsettings.json填写数据库连接字符串，按F5运行即可！

## 引用的开源项目：

1. ##### **`ASP.NET Boilerplate`**—— A STRONG INFRASTRUCTURE FOR MODERN WEB APPLICATIONS。[Abp,MIT开源协议](https://github.com/aspnetboilerplate/aspnetboilerplate)      


## 本项目说明

**1、一切开发请遵照MIT开源协议进行,`ASP.NET Boilerplate`采用的是MIT开源协议、需要用到时请务必在项目介绍加上对应描述并按照相应开源协议要求做出声明及标注。**

**2、有bug欢迎提出issue！或进行评论**

**3、关于数据库脚本问题，请先移步至Scripts文件下，下载Data和Table两个文件，再数据库中先执行Table.sql，再执行Data.sql!**

## 开发目的

在现如今发展迅速的酒店行业，随着酒店的日常工作增加，已经很难用人工去进行处理，一些繁琐的数据也可能会因为人工的失误而造成酒店的一些损失，因此很需要一款可以协助酒店进行内部管理的管理软件。

## 系统开发环境

**操作系统：Windows 10(x64)**

**开发工具：Microsoft Visual Studio 2019(16.9)**

**数据库：MySQL v8.0.22**

**数据库管理工具：Navicat 15**

**开发语言：C#语言、T-SQL语言**

**开发平台：.Net**

**开发框架：.Net 5/ASP.NET Boilerplate**

**开发技术：.NET 5 WebAPI**

## 系统结构

```
Core5.TopSkyHotelManagerSystem
├─ Core5.TopSkyHotelManagerSystem.Application
│    ├─ Core5.TopSkyHotelManagerSystem.Application.csproj
│    ├─ Properties
│    │    └─ AssemblyInfo.cs
│    ├─ TopSkyHotelManagerSystemAppServiceBase.cs
│    ├─ TopSkyHotelManagerSystemApplicationModule.cs
│    ├─ User
│    │    ├─ IUserAppService.cs
│    │    └─ UserAppService.cs
├─ Core5.TopSkyHotelManagerSystem.Core
│    ├─ Configuration
│    │    └─ AppConfigurations.cs
│    ├─ Core5.TopSkyHotelManagerSystem.Core.csproj
│    ├─ Localization
│    │    ├─ SourceFiles
│    │    └─ TopSkyHotelManagerSystemLocalizationConfigurer.cs
│    ├─ Properties
│    │    └─ AssemblyInfo.cs
│    ├─ TopSkyHotelManagerSystemConsts.cs
│    ├─ TopSkyHotelManagerSystemCoreModule.cs
│    ├─ User
│    │    └─ User.cs
│    ├─ Web
│    │    └─ WebContentFolderHelper.cs
├─ Core5.TopSkyHotelManagerSystem.EntityFrameworkCore
│    ├─ Core5.TopSkyHotelManagerSystem.EntityFrameworkCore.csproj
│    ├─ EntityFrameworkCore
│    │    ├─ DbContextOptionsConfigurer.cs
│    │    ├─ TopSkyHotelManagerSystemDbContext.cs
│    │    ├─ TopSkyHotelManagerSystemDbContextFactory.cs
│    │    └─ TopSkyHotelManagerSystemEntityFrameworkCoreModule.cs
│    ├─ Properties
│    │    └─ AssemblyInfo.cs
└─ Core5.TopSkyHotelManagerSystem.Web
       ├─ .bowerrc
       ├─ App_Data
       │    └─ Logs
       ├─ Controllers
       │    ├─ ErrorController.cs
       │    ├─ HomeController.cs
       │    ├─ LayoutController.cs
       │    ├─ TopSkyHotelManagerSystemControllerBase.cs
       │    └─ User
       ├─ Core5.TopSkyHotelManagerSystem.Web.csproj
       ├─ Core5.TopSkyHotelManagerSystem.Web.csproj.user
       ├─ Project_Readme.html
       ├─ Properties
       │    └─ launchSettings.json
       ├─ Startup
       │    ├─ PageNames.cs
       │    ├─ Program.cs
       │    ├─ Startup.cs
       │    ├─ TopSkyHotelManagerSystemNavigationProvider.cs
       │    └─ TopSkyHotelManagerSystemWebModule.cs
       ├─ Utils
       │    └─ UrlHelper.cs
       ├─ Views
       │    ├─ Home
       │    ├─ Shared
       │    ├─ TopSkyHotelManagerSystemRazorPage.cs
       │    ├─ _ViewImports.cshtml
       │    └─ _ViewStart.cshtml
       ├─ app.config
       ├─ appsettings.json
       ├─ bower.json
       ├─ bundleconfig.json
       ├─ compilerconfig.json
       ├─ compilerconfig.json.defaults
       ├─ log4net.Production.config
       ├─ log4net.config
       ├─ web.config
       └─ wwwroot
              ├─ css
              ├─ js
              ├─ lib
              └─ view-resources
```


## 系统功能模块汇总

| 功能汇总       |              |              |              |              |              |              |      |
| -------------- | ------------ | ------------ | ------------ | ------------ | ------------ | ------------ | ---- |
| (前台)客房管理 | 预约房间     | 入住房间     | 结算退房     | 转换房间     | 查看用户信息 | 修改房间状态 |      |
| (前台)用户管理 | 用户信息展示 | 搜索用户信息 | 添加客户     |              |              |              |      |
| (前台)商品消费 | 商品列表     | 搜索商品信息 | 商品消费     | 消费信息     |              |              |      |
| (前台)扩展功能 | 无           |              |              |              |              |              |      |
| (后台)基础信息 | 职位类型维护 | 民族类型维护 | 学历类型维护 | 部门信息维护 |              |              |      |
| (后台)财务信息 | 员工工资账单 | 内部财务账单 | 酒店盈利情况 |              |              |              |      |
| (后台)水电管理 | 水电信息     |              |              |              |              |              |      |
| (后台)监管统计 | 监管部门情况 |              |              |              |              |              |      |
| (后台)客房管理 | 房态图一览   | 新增客房     |              |              |              |              |      |
| (后台)客户管理 | 客户信息管理 | 顾客消费账单 |              |              |              |              |      |
| 功能汇总(续)   |              |              |              |              |              |              |      |
| (后台)人事管理 | 员工管理     | 公告日志     | 上传公告日志 |              |              |              |      |
| (后台)物资管理 | 商品管理     | 仓库物资     |              |              |              |              |      |
| 员工操作日志   |              |              |              |              |              |              |      |

## 项目作者

原作者：

**原创团队：Jackson、Benjamin、Bin、Jonathan**

现维护组织：

**咖啡与网络(Java&Net)**


## 安装教程

1. ## 项目运行部署(进行此步操作前，请先确保系统已安装.Net5最新SDK，可前往https://dotnet.microsoft.com/download/dotnet/5.0)

   **下载并安装Microsoft Visual Studio Professional 2019 v16.9或更新版本，并通过下载Zip包解压，打开.sln后缀格式文件运行。**

   **选中解决方案名称，右键-选择-还原Nuget包**

   ## 数据库运行部署(本地)

   **作者及开发团队强烈建议使用MySQL数据库，安装MySQL数据库并开启服务，通过可视化管理工具对数据库进行建立，可通过打开执行数据库脚本文件夹内的.sql后缀格式文件进行快速建立数据表和导入数据，执行步骤(以MySQL数据库为例)：**

   **1、通过可视化管理工具打开Table.sql文件进行数据表建立。**

   **2、随后打开Data.sql文件进行数据导入。**

## 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

# Star趋势图(感谢Giteye提供的趋势图报表功能！)：
[![Giteye chart](https://chart.giteye.net/gitee/java-and-net/core-topsky-hotel-manager-system/ZQ9CKH6F.png)](https://giteye.net/chart/ZQ9CKH6F)

[![java-and-net/Core5-TopskyHotelManagerSystem](https://gitee.com/java-and-net/core-topsky-hotel-manager-system/widgets/widget_card.svg?colors=4183c4,ffffff,ffffff,e3e9ed,666666,9b9b9b)](https://gitee.com/java-and-net/core-topsky-hotel-manager-system)